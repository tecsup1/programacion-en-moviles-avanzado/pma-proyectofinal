//
//  ViewControllerList.swift
//  PMA-finalProject
//
//  Created by Edgardo Munoz on 7/21/20.
//  Copyright © 2020 tecsup. All rights reserved.
//

import UIKit
import Firebase

struct Reportes {
    var afectado = ""
    var descripcion = ""
    var foto = ""
    var fecha = ""
}

class CardCellController: UITableViewCell {
    
    
    
    @IBOutlet weak var Card: UIView!
   
    @IBOutlet weak var CardImage: UIImageView!
    
    @IBOutlet weak var CardTitle: UILabel!
    
    @IBOutlet weak var CardText: UILabel!
    
    func cambiarCard() {
        Card.layer.cornerRadius = 4.0
        Card.layer.borderWidth = 1.0
        Card.layer.borderColor = UIColor.clear.cgColor
        Card.layer.masksToBounds = false
        Card.layer.shadowColor = UIColor.gray.cgColor
        Card.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        Card.layer.shadowRadius = 4.0
        Card.layer.shadowOpacity = 1.0
        Card.layer.masksToBounds = false
        //Card.layer.shadowPath = UIBezierPath(roundedRect: Card.bounds, cornerRadius: Card.layer.cornerRadius).cgPath
        /*Card.layer.shadowColor = UIColor.black.cgColor
        Card.layer.shadowOpacity = 0.5
        Card.layer.shadowOffset = .zero
        Card.layer.shadowRadius = 9
        Card.layer.cornerRadius = 4.0*/
    }
    
}

class ViewControllerList: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
     var items: [Reportes] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        Database.database().reference().child("reportes").observe(DataEventType.childAdded, with: {snapshot in
            var reporte = Reportes()
            reporte.afectado = (snapshot.value as! NSDictionary)["afectado"] as! String
            reporte.descripcion = (snapshot.value as! NSDictionary)["descripcion"] as! String
            reporte.fecha = (snapshot.value as! NSDictionary)["fecha"] as! String
            reporte.foto = (snapshot.value as! NSDictionary)["foto"] as! String
            self.items.append(reporte)
            self.tableView.reloadData()
            
        }) {error in
             print(error.localizedDescription)
        }

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCellController
        cell.cambiarCard()
        let item = items[indexPath.row]
        //imageView.sd_setImage(with: URL(string: snap.imagenURL), completed: nil)
        cell.CardImage.sd_setImage(with: URL(string: item.foto), completed: nil)
        cell.CardText.text = item.descripcion
        cell.CardTitle.text = item.afectado
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
