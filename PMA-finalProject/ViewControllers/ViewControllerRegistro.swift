//
//  ViewControllerRegistro.swift
//  PMA-finalProject
//
//  Created by Edgardo Munoz on 7/21/20.
//  Copyright © 2020 tecsup. All rights reserved.
//

import UIKit
import Firebase


class ViewControllerRegistro: UIViewController {
    
    
    @IBOutlet weak var txtUsuario: UITextField!
    
    @IBOutlet weak var txtPass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func btnBackTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnRegistroTapped(_ sender: Any) {
        var message = ""
        Auth.auth().createUser(withEmail: txtUsuario.text!, password: txtPass.text!, completion: {(user, error) in
            print("Intendo crear un usuario")
            if error != nil {
                let errCode = AuthErrorCode(rawValue: error!._code)
                print("Se presento el siguiento error al crear el usuario: \(errCode!)")
                switch errCode {
                case .invalidEmail:
                    message = "email invalido"
                case .emailAlreadyInUse:
                    message = "ya existe el usuario"
                case .weakPassword:
                    message = "contraseña poco segura"
                default:
                    print("error desconocido")
                }
                
                let alerta = UIAlertController(title: "Error", message: "Se presento el siguiento error al crear el usuario: \(message)", preferredStyle: .alert)
                
                let btnOK = UIAlertAction(title: "Aceptar", style: .cancel, handler: {(UIAlertAction) in
                    
                })
                
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
            } else {
                print("El usuario fue creado exitosamente")
                Database.database().reference().child("usuarios").child(user!.user.uid).child("email").setValue(user!.user.email)
                
                let alerta = UIAlertController(title: "Creacion de Usuario", message: "Usuario: \(self.txtUsuario.text!) se creo correctamente.", preferredStyle: .alert)
                
                let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {(UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                })
                
                alerta.addAction(btnOK)
                self.present(alerta, animated: true, completion: nil)
            }
        })
    }
}
