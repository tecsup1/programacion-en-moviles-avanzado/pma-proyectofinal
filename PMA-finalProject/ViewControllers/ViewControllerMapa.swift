//
//  ViewControllerMapa.swift
//  PMA-finalProject
//
//  Created by Gustavo Torres Diaz on 7/20/20.
//  Copyright © 2020 tecsup. All rights reserved.
//

import UIKit
import GoogleMaps
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialButtons_Theming
import CoreLocation
import Firebase
import SDWebImage

class ViewControllerMapa: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    var latitud_de_distancia_mas_pequena:Double?
    var longitud_de_distancia_mas_pequena:Double?
    var distancia_mas_pequena:Double?
    var reportes_mas_pequena:Int?
    var color_mas_pequena:String?
    var id_mas_pequena:String?
    
    
    @IBAction func cerrarSesionTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    override func loadView() {
        self.checkServices()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 16.0)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        marker.title = "Current Location"
        marker.map = mapView
        
        Database.database().reference().child("ubicacionCirculos").observe(DataEventType.childAdded, with: {snapshot in
            let latitud = (snapshot.value as! NSDictionary)["latitud_centro_circulo"] as! Double
            let longitud = (snapshot.value as! NSDictionary)["longitud_centro_circulo"] as! Double
            let color_circulo = (snapshot.value as! NSDictionary)["color_circulo"] as! String

            
            let marker:CLLocationCoordinate2D?
            let circ: GMSCircle?
            marker = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
            circ = GMSCircle(position: marker!, radius: 100)
            
            if color_circulo == "green"{
            circ!.fillColor = UIColor(red: 0, green: 1, blue: 0, alpha: 0.3)
            }else if color_circulo == "yellow"{
                circ!.fillColor = UIColor(red: 1, green: 1, blue: 0, alpha: 0.3)
            }else if color_circulo == "red"{
                circ!.fillColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.3)
            }
            
            circ!.map = mapView
                  
        }) {error in
             print(error.localizedDescription)
        }
        
      
        
      /*let marker = GMSMarker()
      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
      marker.title = "Sydney"
      marker.snippet = "Australia"
      marker.map = mapView*/
      
      mapView.delegate = self
      self.view = mapView
    }

    // MARK: GMSMapViewDelegate

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
      print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        self.calcularDistanciaMasPequena(coordinate.latitude, coordinate.longitude ) {
            print("dist más pequeña:\(self.distancia_mas_pequena)")
            if self.distancia_mas_pequena ?? 10000 < 100 {
                self.performSegue(withIdentifier: "segueReport", sender: nil)
                self.latitud_de_distancia_mas_pequena = nil
                self.longitud_de_distancia_mas_pequena = nil
                self.distancia_mas_pequena = nil
                self.reportes_mas_pequena = nil
                self.color_mas_pequena = nil
                self.id_mas_pequena = nil
            }
        }
        
        //self.performSegue(withIdentifier: "segueReport", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let with = UIScreen.main.bounds.width
        let heigh = UIScreen.main.bounds.height
        let floatingButton = MDCFloatingButton()
        floatingButton.frame = CGRect(x: Int(with - 100), y: Int(heigh - 100), width: 68, height: 68)
        floatingButton.setTitle("+", for: .normal)
        floatingButton.setTitleColor(.white, for: .normal)
        floatingButton.titleLabel?.font = floatingButton.titleLabel?.font.withSize(31.0)
        floatingButton.titleLabel?.textAlignment = .center
        floatingButton.backgroundColor = .purple
        floatingButton.setElevation(ShadowElevation(rawValue: 4), for: .normal)
        floatingButton.addTarget(self, action: #selector(btnFloatingButtonTapped), for: .touchUpInside)
        
        
        let floatingButton2 = MDCFloatingButton()
        floatingButton2.frame = CGRect(x: Int(with - 180), y: Int(heigh - 100), width: 68, height: 68)
        floatingButton2.setTitle("info", for: .normal)
        floatingButton2.setTitleColor(.white, for: .normal)
        floatingButton2.titleLabel?.font = floatingButton.titleLabel?.font.withSize(19.0)
        floatingButton2.titleLabel?.textAlignment = .center
        floatingButton2.backgroundColor = .purple
        floatingButton2.setElevation(ShadowElevation(rawValue: 4), for: .normal)
        floatingButton2.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(floatingButton)
        self.view.addSubview(floatingButton2)
    
        
        
        
        /*let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(mapView)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        
        //let circleCenter = CLLocationCoordinate2D(latitude: 37.35, longitude: -122.0)
        let circ = GMSCircle(position: marker.position, radius: 1000)
        circ.fillColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.3)
        circ.map = mapView
        
        self.view.addSubview(floatingButton)*/
        
       
        // Do any additional setup after loading the view.
    }

    

    func calcularDistanciaMasPequena(_ latitud_de_clic: Double,_ longitud_de_clic: Double, finished: @escaping () -> Void){

         //Database.database().reference().child("ubicacionCirculos").observeSingleEvent(of: .childAdded, with: {snapshot in
         Database.database().reference().child("ubicacionCirculos").observe( DataEventType.childAdded, with:{ snapshot in
             //print(snapshot)
    

             let latitud_de_circulo_get = (snapshot.value as! NSDictionary)["latitud_centro_circulo"] as! Double
             let longitud_de_circulo_get = (snapshot.value as! NSDictionary)["longitud_centro_circulo"] as! Double
             let reportes_dentro_de_circulo = (snapshot.value as! NSDictionary)["reportes_dentro_de_circulo"] as! Int
             let color_circulo = (snapshot.value as! NSDictionary)["color_circulo"] as! String
             
             let coordenadas_de_puntos_recorridos = CLLocation(latitude: latitud_de_circulo_get, longitude: longitud_de_circulo_get)
             let coordenada_actual = CLLocation(latitude: latitud_de_clic, longitude: longitud_de_clic)
             
             let distancia_entre_puntos:Double = coordenada_actual.distance(from: coordenadas_de_puntos_recorridos)
             if distancia_entre_puntos <  self.distancia_mas_pequena ?? 1000000000{
                 self.id_mas_pequena = snapshot.key
                 self.distancia_mas_pequena = distancia_entre_puntos
                 self.latitud_de_distancia_mas_pequena = latitud_de_circulo_get
                 self.longitud_de_distancia_mas_pequena = longitud_de_circulo_get
                 self.reportes_mas_pequena = reportes_dentro_de_circulo
                 self.color_mas_pequena = color_circulo
             }
             
             DispatchQueue.main.async {
                 finished()
             }
         })
     }
    
    
    
    
    
    
    func checkServices() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            checkLocationAutorization()
        } else {
            
        }
    }
    
    func checkLocationAutorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            print("AUTHORIZEWHENINUSE")
            locationManager.startUpdatingLocation()

            //print((locationManager.location?.coordinate.latitude)! as Double)
            //self.lat = -16.4460391//
            //self.lon = -71.5548963//
            self.lat = (locationManager.location?.coordinate.latitude)! as Double
            self.lon = (locationManager.location?.coordinate.longitude)! as Double
            break
        case .denied:
            print("DENIED")
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            print("NOTDETERMINED")
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            print("RESTRICTED")
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            print("AUTHORIZEDALWAYS")
            break
        
        default:
            break
        }
    }
    
    @objc func btnFloatingButtonTapped() {
        self.performSegue(withIdentifier: "segueAdd", sender: [self.lat, self.lon])
    }
    
    @objc func infoButtonTapped() {
        self.performSegue(withIdentifier: "showInfoSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueAdd" {
            let siguienteVC = segue.destination as! ViewControllerFormulario
            siguienteVC.coordenadas = sender as! [Double]
        }
        
    }
    


}
