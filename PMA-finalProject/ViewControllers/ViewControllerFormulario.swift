//
//  ViewControllerFormulario.swift
//  PMA-finalProject
//
//  Created by Edgardo Munoz on 7/21/20.
//  Copyright © 2020 tecsup. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class ViewControllerFormulario: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var txtPersona: UITextField!
    @IBOutlet weak var txtDescripcion: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    var coordenadas: [Double] = []
    
    
    var latitud_actual:Double?
    
    var longitud_actual:Double?
    
    var latitud_de_distancia_mas_pequena:Double?
    var longitud_de_distancia_mas_pequena:Double?
    var distancia_mas_pequena:Double?
    var reportes_mas_pequena:Int?
    var color_mas_pequena:String?
    var id_mas_pequena:String?
    var array_de_ids_de_circulos_mas_pequena:[String]?
    
    var id_de_circulo:String?
    
    var latitud_anterior:Double?
    var longitud_anterior:Double?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        print(coordenadas)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnRegistrarTapped(_ sender: Any) {
        let imagenesFolder = Storage.storage().reference().child("fotos")
        let imagenData = imageView.image?.jpegData(compressionQuality: 0.50)
        let cargarImagen = imagenesFolder.child("\(NSUUID().uuidString).jpg")
        
        let observable = cargarImagen.putData(imagenData!, metadata: nil) {
            (metadata, error) in
                if error != nil {
                    self.mostrarAlerta(titulo: "Error", mensaje: "Se produjo un error al subir la imagen. Verifique su conexion a internet y vuelva a intentarlo", accion: "Aceptar")
                    print("Ocurrio un error al subir imagen: \(error!) ")
                } else {
                    self.latitud_actual = self.coordenadas[0]
                    self.longitud_actual = self.coordenadas[1]
                    
                    self.id_de_circulo = Database.database().reference().child("reportes").childByAutoId().key
                    
                    self.calcularDistanciaMasPequena{ self.actualizarOCrearCirculo() }
                    //Database.database().reference().child("ubicacionCirculos").removeAllObservers()
                    
                    cargarImagen.downloadURL(completion: {(url, error) in
                        guard let enlaceURL = url else {
                            self.mostrarAlerta(titulo: "Error", mensaje: "Se produjo un error al obtener informacion de imagen.", accion: "Cancelar")
                            print("Ocurrio un error al obtener informacion de imagen \(error!)")
                            return
                        }
                        let URL = enlaceURL.absoluteString
                        let a = Date().description
                        let index = a.index(a.startIndex, offsetBy: 10)
                        let currentDate = a[..<index]
                        let data = ["afectado": self.txtPersona.text!, "descripcion": self.txtDescripcion.text!, "foto": URL, "fecha": currentDate, "latitud": self.coordenadas[0], "longitud": self.coordenadas[1]] as Dictionary<String, Any>
                        
                        
                        Database.database().reference().child("reportes/\(self.id_de_circulo!)").setValue(data)
                        //self.performSegue(withIdentifier: "seleccionarContactoSegue", sender: data)
                        
                    })
                    
            }
        }
        
        
        let alertaCarga = UIAlertController(title: "Cargando Imagen . . .", message: "0%", preferredStyle: .alert)
        let progresoCarga: UIProgressView = UIProgressView(progressViewStyle: .default)
        observable.observe(.progress) { (snapshot) in
            let porcentaje = Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)
            print(porcentaje)
            progresoCarga.setProgress(Float(porcentaje), animated: true)
            progresoCarga.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
            alertaCarga.message = String(round(porcentaje*100.0)) + " %"
            /*if porcentaje >= 1.0 {
                alertaCarga.dismiss(animated: true, completion: nil)
                self.performSegue(withIdentifier: "toListSegue", sender: nil)
            }*/
        }
        
        let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {_ in
            self.performSegue(withIdentifier: "volverMapaSegue", sender: nil)
        })
        alertaCarga.addAction(btnOK)
        alertaCarga.view.addSubview(progresoCarga)
        present(alertaCarga, animated: true, completion: nil)
        
        
    }
    
    func actualizarOCrearCirculo(){
        if (self.distancia_mas_pequena! < 100){
        // actualizando coordenada de circulo con punto medio
           let latitud_punto_medio = (self.latitud_de_distancia_mas_pequena! + self.latitud_actual!)/2
           let longitud_punto_medio =  (self.longitud_de_distancia_mas_pequena! + self.longitud_actual!)/2
           
           // actualizar nuevo punto de circulo
            
            self.reportes_mas_pequena! += 1
            if self.reportes_mas_pequena! >= 5 {
                self.color_mas_pequena! = "red"
            }else if self.reportes_mas_pequena! >= 3{
                self.color_mas_pequena! = "yellow"
            }else if self.reportes_mas_pequena! >= 1{
                self.color_mas_pequena! = "green"
            }
            
            var miArray:[String] = []
            miArray = self.array_de_ids_de_circulos_mas_pequena ?? []
            
            if(miArray.last != self.id_mas_pequena! ){
                
                miArray.append(self.id_mas_pequena!)

            }
            let data = ["color_circulo": self.color_mas_pequena!, "latitud_centro_circulo": latitud_punto_medio, "longitud_centro_circulo": longitud_punto_medio, "reportes_dentro_de_circulo": self.reportes_mas_pequena!, "array_de_ids_de_circulos": miArray] as Dictionary<String, Any>
            print("MIDATA: \(data)")
            latitud_anterior = latitud_punto_medio
            longitud_anterior = longitud_punto_medio
           Database.database().reference().child("ubicacionCirculos/\(self.id_mas_pequena!)").setValue(data)
        }else{
           // generando nueva coordenada de circulo
            latitud_anterior = self.latitud_actual
            longitud_anterior = self.longitud_actual
            let data = ["color_circulo": "green", "latitud_centro_circulo": self.latitud_actual, "longitud_centro_circulo": self.longitud_actual,"reportes_dentro_de_circulo": 1, "array_de_ids_de_circulos": []] as Dictionary<String, Any>
           Database.database().reference().child("ubicacionCirculos").childByAutoId().setValue(data)
        }
    }
    
    
    func calcularDistanciaMasPequena(finished: @escaping () -> Void){

        //Database.database().reference().child("ubicacionCirculos").observeSingleEvent(of: .childAdded, with: {snapshot in
        Database.database().reference().child("ubicacionCirculos").observe( DataEventType.childAdded, with:{ snapshot in
            print(snapshot)
            

            let latitud_de_circulo_get = (snapshot.value as! NSDictionary)["latitud_centro_circulo"] as! Double
            let longitud_de_circulo_get = (snapshot.value as! NSDictionary)["longitud_centro_circulo"] as! Double
            //print("latitud_actual:\(self.latitud_actual)")

            
            let miRespuesta = Database.database().reference().child("ubicacionCirculos").queryLimited(toLast: UInt(1))
            print("mirespuesta:\(miRespuesta)")
            
            if self.latitud_actual != latitud_de_circulo_get && self.longitud_actual != longitud_de_circulo_get {
                
                let reportes_dentro_de_circulo = (snapshot.value as! NSDictionary)["reportes_dentro_de_circulo"] as! Int
                let color_circulo = (snapshot.value as! NSDictionary)["color_circulo"] as! String
                let array_de_ids_de_circulos = (snapshot.value as! NSDictionary)["array_de_ids_de_circulos"] as! [String]?
                
                
                let coordenadas_de_puntos_recorridos = CLLocation(latitude: latitud_de_circulo_get, longitude: longitud_de_circulo_get)
                let coordenada_actual = CLLocation(latitude: self.latitud_actual!, longitude: self.longitud_actual!)
                
                let distancia_entre_puntos:Double = coordenada_actual.distance(from: coordenadas_de_puntos_recorridos)
                if distancia_entre_puntos <  self.distancia_mas_pequena ?? 1000000000{
                    self.id_mas_pequena = snapshot.key
                    self.distancia_mas_pequena = distancia_entre_puntos
                    self.latitud_de_distancia_mas_pequena = latitud_de_circulo_get
                    self.longitud_de_distancia_mas_pequena = longitud_de_circulo_get
                    self.reportes_mas_pequena = reportes_dentro_de_circulo
                    self.color_mas_pequena = color_circulo
                    self.array_de_ids_de_circulos_mas_pequena = array_de_ids_de_circulos ?? []
                }
                
                DispatchQueue.main.async {
                    
                    finished()
                }
                
            }
        })
    }
    
    
    @IBAction func btnCancelarTapped(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnCameraTapped(_ sender: Any) {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBAction func btnGaleryTapped(_ sender: Any) {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imageView.image = image
        imageView.backgroundColor = UIColor.clear
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func mostrarAlerta(titulo: String, mensaje: String, accion: String) {
        let alerta = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        let btnCANCELOK = UIAlertAction(title: accion, style: .default, handler: nil)
        alerta.addAction(btnCANCELOK)
        present(alerta, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
