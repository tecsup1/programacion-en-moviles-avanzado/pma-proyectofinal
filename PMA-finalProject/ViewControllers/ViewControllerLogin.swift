//
//  ViewController.swift
//  PMA-finalProject
//
//  Created by Gonzalo Quispe Fernandez on 7/14/20.
//  Copyright © 2020 tecsup. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn

class ViewControllerLogin: UIViewController, GIDSignInDelegate {
    
    

    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btnIngresar: UIButton!
    @IBOutlet weak var btnRegistrar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnIngresar.layer.cornerRadius = 7.0
        btnRegistrar.layer.cornerRadius = 7.0
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
   
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil && user.authentication != nil {
            
            let credential = GoogleAuthProvider.credential(withIDToken: user.authentication.idToken, accessToken: user.authentication.accessToken)
            
            Auth.auth().signIn(with: credential) {(user, error) in
                if error != nil {
                    print("Se presento el siguiente Error: \(error!)")
                } else {
                    self.performSegue(withIdentifier: "segueLogeo", sender: nil)
                }
            }
            
        }
    }

    @IBAction func loginWithGoogleTapped(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func ingresarTapped(_ sender: Any) {
        //self.performSegue(withIdentifier: "segueLogeo", sender: nil)
        Auth.auth().signIn(withEmail: txtUsuario.text!, password: txtPass.text!) {(user, error) in
            print("Intentado Iniciar Sesion")
            print("@@@@@@@@@ \(self.txtUsuario.text!) @@@@@@")
            print("@@@@@@@@@ \(self.txtPass.text!) @@@@@@")
            if error != nil {
                print("Se presento el siguiente Error: \(error!)")
                self.mostrarAlerta(titulo: "Crear Usuario", mensaje: "Crear nuevo usuario")
            } else {
                print("Inicio de sesion exitoso")
                self.performSegue(withIdentifier: "segueLogeo", sender: nil)
            }
        }
    }
    
    func mostrarAlerta(titulo: String, mensaje: String) {
        let alerta = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        let btnCrear = UIAlertAction(title: "Crear", style: .default, handler: {(_) in
            self.performSegue(withIdentifier: "seguecrearusuario", sender: nil)
            //alerta.dismiss(animated: true, completion: nil)
        })
        let btnCancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alerta.addAction(btnCrear)
        alerta.addAction(btnCancelar)
        present(alerta, animated: true, completion: nil)
    }
}

